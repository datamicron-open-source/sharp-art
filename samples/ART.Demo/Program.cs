﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;

namespace ART
{
    public class Program
    {
        public static void Main(string[] args)
        {
            sample_001();
            sample_002();
        }

        static void sample_001()
        {
            var t3 = new ArtTree();

            string s = "api.foo.bar";
            t3.Insert(Encoding.ASCII.GetBytes(s).Append((byte)0).ToArray(), s);
            s = "api.foo.baz";
            t3.Insert(Encoding.ASCII.GetBytes(s).Append((byte)0).ToArray(), s);
            s = "api.foe.fum";
            t3.Insert(Encoding.ASCII.GetBytes(s).Append((byte)0).ToArray(), s);
            s = "abc.123.456";
            t3.Insert(Encoding.ASCII.GetBytes(s).Append((byte)0).ToArray(), s);
            s = "api.foo";
            t3.Insert(Encoding.ASCII.GetBytes(s).Append((byte)0).ToArray(), s);
            s = "api";
            t3.Insert(Encoding.ASCII.GetBytes(s).Append((byte)0).ToArray(), s);

            var expected = new string[] { "api", "api.foe.fum", "api.foo", "api.foo.bar", "api.foo.baz" };

            using (var prefIter = t3.GetPrefixIterator(Encoding.ASCII.GetBytes("api")))
            {
                var i = 0;
                while (prefIter.MoveNext())
                {
                    var value = prefIter.Current.Value;
                    Debug.Assert(expected[i] == value.ToString());
                    i++;
                }
                Debug.Assert(expected.Length == i);
            }
        }


        static void sample_002()
        {
            var t3 = new ArtTree();

            int s = 9;
            t3.Insert(BitConverter.GetBytes(s), s);
            s = 5;
            t3.Insert(BitConverter.GetBytes(s), s);
            s = 2;
            t3.Insert(BitConverter.GetBytes(s), s);
            s = 1;
            t3.Insert(BitConverter.GetBytes(s), s);
            s = 6;
            t3.Insert(BitConverter.GetBytes(s), s);
            s = 0;
            t3.Insert(BitConverter.GetBytes(s), s);
            s = 1000000;
            t3.Insert(BitConverter.GetBytes(s), s);

            var expected = new int[] { 0, 1, 2, 5, 6, 9, 1000000 };
            using (var iter = t3.GetIterator())
            {
                var i = 0;
                while (iter.MoveNext())
                {
                    var value = iter.Current.Value;
                    Debug.Assert(expected[i] == (int)value);
                    i++;
                }

                Debug.Assert(expected.Length == i);
            }
        }

    }
}
