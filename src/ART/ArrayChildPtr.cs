﻿namespace ART
{
    public class ArrayChildPtr : ChildPtr
    {
        private Node[] children;
        private readonly int i;

        public ArrayChildPtr(Node[] children, int i)
        {
            this.children = children;
            this.i = i;
        }

        public override Node Get()
        {
            return children[i];
        }

        public override void Set(Node n)
        {
            children[i] = n;
        }
    }
}
