﻿using System;

namespace ART
{
    public abstract class ArtNode : Node
    {
        public int num_children = 0;
        public int partial_len = 0;
        public readonly byte[] partial = new byte[MAX_PREFIX_LEN];

        public ArtNode()
            : base()
        {

        }

        public ArtNode(ArtNode other)
            : base(other)
        {
            this.num_children = other.num_children;
            this.partial_len = other.partial_len;
            Array.Copy(other.partial, 0, partial, 0, Math.Min(MAX_PREFIX_LEN, partial_len));
        }

        /**
         * Returns the number of prefix characters shared between
         * the key and node.
         */
        public int check_prefix(byte[] key, int depth)
        {
            var max_cmp = Math.Min(Math.Min(partial_len, MAX_PREFIX_LEN), key.Length - depth);
            int idx;
            for (idx = 0; idx < max_cmp; idx++)
            {
                if (partial[idx] != key[depth + idx])
                    return idx;
            }
            return idx;
        }

        /**
     * Calculates the index at which the prefixes mismatch
     */
        public int prefix_mismatch(byte[] key, int depth)
        {
            var max_cmp = Math.Min(Math.Min(MAX_PREFIX_LEN, partial_len), key.Length - depth);
            int idx;
            for (idx = 0; idx < max_cmp; idx++)
            {
                if (partial[idx] != key[depth + idx])
                    return idx;
            }

            // If the prefix is short we can avoid finding a leaf
            if (partial_len > MAX_PREFIX_LEN)
            {
                // Prefix is longer than what we've checked, find a leaf
                var l = this.minimum();
                max_cmp = Math.Min(l.key.Length, key.Length) - depth;
                for (; idx < max_cmp; idx++)
                {
                    if (l.key[idx + depth] != key[depth + idx])
                        return idx;
                }
            }
            return idx;
        }

        public abstract ChildPtr find_child(byte c);

        public abstract void add_child(ChildPtr _ref, byte c, Node child);

        public abstract void remove_child(ChildPtr _ref, byte c);

        // Precondition: isLastChild(i) == false
        public abstract int nextChildAtOrAfter(int i);

        public abstract Node childAt(int i);

        public override bool insert(ChildPtr _ref, byte[] key, object value, int depth, bool force_clone)
        {
            var do_clone = force_clone || this.refcount > 1;

            // Check if given node has a prefix
            if (partial_len > 0)
            {
                // Determine if the prefixes differ, since we need to split
                int prefix_diff = prefix_mismatch(key, depth);
                if (prefix_diff >= partial_len)
                {
                    depth += partial_len;
                }
                else
                {
                    // Create a new node
                    var new_node = new ArtNode4();
                    var ref_old = _ref.Get();
                    _ref.change_no_decrement(new_node); // don't decrement yet, because doing so might destroy self
                    new_node.partial_len = prefix_diff;
                    Array.Copy(partial, 0, new_node.partial, 0, Math.Min(MAX_PREFIX_LEN, prefix_diff));

                    // Adjust the prefix of the old node
                    var _this_writable = do_clone ? (ArtNode)this.n_clone() : this;

                    if (partial_len <= MAX_PREFIX_LEN)
                    {
                        new_node.add_child(_ref, _this_writable.partial[prefix_diff], _this_writable);
                        _this_writable.partial_len -= (prefix_diff + 1);
                        Array.Copy(_this_writable.partial, prefix_diff + 1, _this_writable.partial, 0, Math.Min(MAX_PREFIX_LEN, _this_writable.partial_len));
                    }
                    else
                    {
                        _this_writable.partial_len -= (prefix_diff + 1);
                        var _l = this.minimum();
                        new_node.add_child(_ref, _l.key[depth + prefix_diff], _this_writable);
                        Array.Copy(_l.key, depth + prefix_diff + 1, _this_writable.partial, 0, Math.Min(MAX_PREFIX_LEN, _this_writable.partial_len));
                    }

                    // Insert the new leaf
                    var l = new Leaf(key, value);
                    new_node.add_child(_ref, key[depth + prefix_diff], l);

                    ref_old.decrement_refcount();

                    return true;
                }
            }

            // Clone self if necessary
            ArtNode this_writable = do_clone ? (ArtNode)this.n_clone() : this;
            if (do_clone)
            {
                _ref.change(this_writable);
            }
            // Do the insert, either in a child (if a matching child already exists) or in self
            var child = this_writable.find_child(key[depth]);
            if (child != null)
            {
                return Node.insert(child.Get(), child, key, value, depth + 1, force_clone);
            }
            else
            {
                // No child, node goes within us
                var l = new Leaf(key, value);
                this_writable.add_child(_ref, key[depth], l);
                // If `this` was full and `do_clone` is true, we will clone a full node
                // and then immediately delete the clone in favor of a larger node.
                // TODO: avoid this
                return true;
            }
        }

        public override bool delete(ChildPtr _ref, byte[] key, int depth, bool force_clone)
        {
            // Bail if the prefix does not match
            if (partial_len > 0)
            {
                int prefix_len = check_prefix(key, depth);
                if (prefix_len != Math.Min(MAX_PREFIX_LEN, partial_len))
                {
                    return false;
                }
                depth += partial_len;
            }

            bool do_clone = force_clone || this.refcount > 1;

            // Clone self if necessary. Note: this allocation will be wasted if the
            // key does not exist in the child's subtree
            var this_writable = do_clone ? (ArtNode)this.n_clone() : this;

            // Find child node
            var child = this_writable.find_child(key[depth]);
            if (child == null) return false; // when translating to C++, make sure to delete this_writable

            if (do_clone)
            {
                _ref.change(this_writable);
            }

            var child_is_leaf = child.Get() is Leaf;
            var do_delete = child.Get().delete(child, key, depth + 1, do_clone);

            if (do_delete && child_is_leaf)
            {
                // The leaf to delete is our child, so we must remove it
                this_writable.remove_child(_ref, key[depth]);
            }

            return do_delete;
        }
    }
}
