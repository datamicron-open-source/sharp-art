﻿using System;

namespace ART
{
    public class Leaf : Node
    {
        public static int count;

        public object value;
        public readonly byte[] key;

        public Leaf(byte[] key, object value)
            : base()
        {

            this.key = key;
            this.value = value;
            count++;
        }

        public Leaf(Leaf other)
            : base(other)
        {
            this.key = other.key;
            this.value = other.value;
            count++;
        }

        public override Node n_clone()
        {
            return new Leaf(this);
        }

        public bool matches(byte[] key)
        {
            if (this.key.Length != key.Length) return false;

            for (int i = 0; i < key.Length; i++)
            {
                if (this.key[i] != key[i])
                {
                    return false;
                }
            }
            return true;
        }

        public bool prefix_matches(byte[] prefix)
        {
            if (this.key.Length < prefix.Length) return false;
            for (int i = 0; i < prefix.Length; i++)
            {
                if (this.key[i] != prefix[i])
                {
                    return false;
                }
            }
            return true;
        }

        public override Leaf minimum()
        {
            return this;
        }

        public int longest_common_prefix(Leaf other, int depth)
        {
            var max_cmp = Math.Min(key.Length, other.key.Length) - depth;
            int idx;
            for (idx = 0; idx < max_cmp; idx++)
            {
                if (key[depth + idx] != other.key[depth + idx])
                {
                    return idx;
                }
            }
            return idx;
        }


        public override bool insert(ChildPtr _ref, byte[] key, object value, int depth, bool force_clone)
        {
            var clone = force_clone || this.refcount > 1;

            if (matches(key))
            {
                if (clone)
                {
                    // Updating an existing value, but need to create a new leaf to
                    // reflect the change
                    _ref.change(new Leaf(key, value));
                }
                else
                {
                    // Updating an existing value, and safe to make the change in
                    // place
                    this.value = value;
                }
                return false;
            }
            else
            {
                // New value

                // Create a new leaf
                var l2 = new Leaf(key, value);

                // Determine longest prefix
                var longest_prefix = longest_common_prefix(l2, depth);
                if (depth + longest_prefix >= this.key.Length || depth + longest_prefix >= key.Length)
                {
                    throw new NotSupportedException("keys cannot be prefixes of other keys");
                }

                // Split the current leaf into a node4
                var new_node = new ArtNode4();
                new_node.partial_len = longest_prefix;
                var ref_old = _ref.Get();
                _ref.change_no_decrement(new_node);
                Array.Copy(key, depth, new_node.partial, 0, Math.Min(MAX_PREFIX_LEN, longest_prefix));

                // Add the leafs to the new node4
                new_node.add_child(_ref, this.key[depth + longest_prefix], this);
                new_node.add_child(_ref, l2.key[depth + longest_prefix], l2);

                ref_old.decrement_refcount();

                // TODO: avoid the increment to self immediately followed by decrement

                return true;
            }
        }


        public override bool delete(ChildPtr _ref, byte[] key, int depth, bool force_clone)
        {
            return matches(key);
        }

        public override bool exhausted(int i)
        {
            return i > 0;
        }

        public override int decrement_refcount()
        {
            if (--this.refcount <= 0)
            {
                count--;
                // delete this;
                // Don't delete the actual key or value because they may be used
                // elsewhere
                return 32;
                // object size (8) + refcount (4) + pointer to key array (8) +
                // pointer to value (8) + padding (4)
            }
            return 0;
        }


    }
}
