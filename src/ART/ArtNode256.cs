﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ART
{
    public class ArtNode256 : ArtNode
    {
        public static int count;

        public Node[] children = new Node[256];


        public ArtNode256()
            : base()
        {
            count++;
        }

        public ArtNode256(ArtNode256 other)
            : base(other)
        {
            for (int i = 0; i < 256; i++)
            {
                children[i] = other.children[i];
                if (children[i] != null)
                {
                    children[i].refcount++;
                }
            }
            count++;
        }

        public ArtNode256(ArtNode48 other)
            : this()
        {
            // ArtNode
            this.num_children = other.num_children;
            this.partial_len = other.partial_len;
            Array.Copy(other.partial, 0, this.partial, 0, Math.Min(MAX_PREFIX_LEN, this.partial_len));
            // ArtNode256 from ArtNode48
            for (int i = 0; i < 256; i++)
            {
                if (other.keys[i] != 0)
                {
                    children[i] = other.children[to_uint(other.keys[i]) - 1];
                    children[i].refcount++;
                }
            }
        }

        public override Node n_clone()
        {
            return new ArtNode256(this);
        }

        public override ChildPtr find_child(byte c)
        {
            if (children[to_uint(c)] != null) return new ArrayChildPtr(children, to_uint(c));
            return null;
        }

        public override Leaf minimum()
        {
            int idx = 0;
            while (children[idx] == null) idx++;
            return Node.minimum(children[idx]);
        }

        public override void add_child(ChildPtr _ref, byte c, Node child)
        {
            Debug.Assert(refcount <= 1);

            this.num_children++;
            this.children[to_uint(c)] = child;
            child.refcount++;
        }

        public override void remove_child(ChildPtr _ref, byte c)
        {
            Debug.Assert(refcount <= 1);

            children[to_uint(c)].decrement_refcount();
            children[to_uint(c)] = null;
            num_children--;

            if (num_children == 37)
            {
                ArtNode48 result = new ArtNode48(this);
                _ref.change(result);
            }
        }

        public override bool exhausted(int c)
        {
            for (int i = c; i < 256; i++)
            {
                if (children[i] != null)
                {
                    return false;
                }
            }
            return true;
        }

        public override int nextChildAtOrAfter(int c)
        {
            int pos = c;
            for (; pos < 256; pos++)
            {
                if (children[pos] != null)
                {
                    break;
                }
            }
            return pos;
        }

        public override Node childAt(int pos)
        {
            return children[pos];
        }

        public override int decrement_refcount()
        {
            if (--this.refcount <= 0)
            {
                int freed = 0;
                for (int i = 0; i < 256; i++)
                {
                    if (children[i] != null)
                    {
                        freed += children[i].decrement_refcount();
                    }
                }
                count--;
                // delete this;
                return freed + 2120;
                // object size (8) + refcount (4) +
                // num_children int (4) + partial_len int (4) +
                // pointer to partial array (8) + partial array size (8+4+1*MAX_PREFIX_LEN)
                // pointer to children array (8) + children array size (8+4+8*256) +
                // padding (4)
            }
            return 0;
        }

    }
}
