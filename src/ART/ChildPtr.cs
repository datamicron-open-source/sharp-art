﻿namespace ART
{
    public abstract class ChildPtr
    {
        public abstract Node Get();
        public abstract void Set(Node n);

        public void change(Node n)
        {
            // First increment the refcount of the new node, in case it would
            // otherwise have been deleted by the decrement of the old node
            n.refcount++;
            if (Get() != null)
            {
                Get().decrement_refcount();
            }
            Set(n);
        }

        public void change_no_decrement(Node n)
        {
            n.refcount++;
            Set(n);
        }
    }
}
