﻿using System;
using System.Collections.Generic;

namespace ART
{
    public class ArtTree : ChildPtr
    {
        Node root = null;
        long num_elements = 0;

        public ArtTree()
        {
        }

        public ArtTree(ArtTree other)
        {
            root = other.root;
            num_elements = other.num_elements;
        }

        public ArtTree Snapshot()
        {
            var b = new ArtTree();
            if (root != null)
            {
                b.root = Node.n_clone(root);
                b.root.refcount++;
            }
            b.num_elements = num_elements;
            return b;
        }

        public override Node Get()
        {
            return root;
        }

        public override void Set(Node n)
        {
            root = n;
        }

        public object Search(byte[] key)
        {
            Node n = root;
            int prefix_len, depth = 0;
            while (n != null)
            {
                if (n is Leaf)
                {
                    var l = (Leaf)n;
                    // Check if the expanded path matches
                    if (l.matches(key))
                    {
                        return l.value;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    var an = (ArtNode)(n);

                    // Bail if the prefix does not match
                    if (an.partial_len > 0)
                    {
                        prefix_len = an.check_prefix(key, depth);
                        if (prefix_len != Math.Min(Node.MAX_PREFIX_LEN, an.partial_len))
                        {
                            return null;
                        }
                        depth += an.partial_len;
                    }

                    if (depth >= key.Length) return null;

                    // Recursively search
                    ChildPtr child = an.find_child(key[depth]);
                    n = (child != null) ? child.Get() : null;
                    depth++;
                }
            }
            return null;
        }

        public void Insert(byte[] key, object value)
        {
            if (Node.insert(root, this, key, value, 0, true))
                num_elements++;
        }

        public void Delete(byte[] key)
        {
            if (root != null)
            {
                var child_is_leaf = root is Leaf;
                var do_delete = root.delete(this, key, 0, false);
                if (do_delete)
                {
                    num_elements--;
                    if (child_is_leaf)
                    {
                        // The leaf to delete is the root, so we must remove it
                        root = null;
                    }
                }
            }
        }


        public ArtIterator GetIterator()
        {
            return new ArtIterator(root);
        }

        public ArtIterator GetPrefixIterator(byte[] prefix)
        {
            // Find the root node for the prefix
            Node n = root;
            int prefix_len, depth = 0;
            while (n != null)
            {
                if (n is Leaf)
                {
                    Leaf l = (Leaf)n;
                    // Check if the expanded path matches
                    if (l.prefix_matches(prefix))
                    {
                        return new ArtIterator(l);
                    }
                    else
                    {
                        return new ArtIterator(null);
                    }
                }
                else
                {
                    if (depth == prefix.Length)
                    {
                        // If we have reached appropriate depth, return the iterator
                        if (n.minimum().prefix_matches(prefix))
                        {
                            return new ArtIterator(n);
                        }
                        else
                        {
                            return new ArtIterator(null);
                        }
                    }
                    else
                    {
                        ArtNode an = (ArtNode)(n);

                        // Bail if the prefix does not match
                        if (an.partial_len > 0)
                        {
                            prefix_len = an.prefix_mismatch(prefix, depth);
                            if (prefix_len == 0)
                            {
                                // No match, return empty
                                return new ArtIterator(null);
                            }
                            else if (depth + prefix_len == prefix.Length)
                            {
                                // Prefix match, return iterator
                                return new ArtIterator(n);
                            }
                            else
                            {
                                // Full match, go deeper
                                depth += an.partial_len;
                            }
                        }

                        // Recursively search
                        ChildPtr child = an.find_child(prefix[depth]);
                        n = (child != null) ? child.Get() : null;
                        depth++;
                    }
                }
            }
            return new ArtIterator(null);
        }

        public long Size()
        {
            return num_elements;
        }

        public int Destroy()
        {
            if (root != null)
            {
                int result = root.decrement_refcount();
                root = null;
                return result;
            }
            else
            {
                return 0;
            }
        }

    }
}
