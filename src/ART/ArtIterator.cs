﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ART
{
    public class ArtIterator : IEnumerator<KeyValuePair<byte[], object>>
    {
        private Stack<Node> elemStack = new Stack<Node>();
        private Stack<int> idxStack = new Stack<int>();

        public ArtIterator(Node root)
        {
            if (root != null)
            {
                elemStack.Push(root);
                idxStack.Push(0);
                maybeAdvance();
            }
        }

        public KeyValuePair<byte[], object> Current
        {
            get
            {
                var leaf = (Leaf)elemStack.Peek();
                byte[] key = leaf.key;
                var value = leaf.value;

                // Mark the leaf as consumed
                idxStack.Push(idxStack.Pop() + 1);

                maybeAdvance();
                return new KeyValuePair<byte[], object>(key, value);
            }
        }

        object IEnumerator.Current { get { return Current; } }

        public bool MoveNext()
        {
            return elemStack.Count > 0;
        }

        public void Reset() { }

        public void Dispose() { }

        private void maybeAdvance()
        {
            // Pop exhausted nodes
            while (elemStack.Count > 0 && elemStack.Peek().exhausted(idxStack.Peek()))
            {
                elemStack.Pop();
                idxStack.Pop();

                if (elemStack.Count > 0)
                {
                    // Move on by advancing the exhausted node's parent
                    idxStack.Push(idxStack.Pop() + 1);
                }
            }

            if (elemStack.Count > 0)
            {
                // Descend to the next leaf node element
                while (true)
                {
                    if (elemStack.Peek() is Leaf)
                    {
                        // Done - reached the next element
                        break;
                    }
                    else
                    {
                        // Advance to the next child of this node
                        ArtNode cur = (ArtNode)elemStack.Peek();
                        idxStack.Push(cur.nextChildAtOrAfter(idxStack.Pop()));
                        Node child = cur.childAt(idxStack.Peek());

                        // Push it onto the stack
                        elemStack.Push(child);
                        idxStack.Push(0);
                    }
                }
            }
        }
    }
}
