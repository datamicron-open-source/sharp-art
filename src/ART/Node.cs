﻿namespace ART
{
    public abstract class Node
    {
        public static readonly int MAX_PREFIX_LEN = 8;

        public int refcount;

        public Node()
        {
            refcount = 0;
        }

        public Node(Node other)
        {
            refcount = 0;
        }

        public abstract Node n_clone();
        public static Node n_clone(Node n)
        {
            if (n == null) return null;
            else return n.n_clone();
        }

        public abstract Leaf minimum();
        public static Leaf minimum(Node n)
        {
            if (n == null) return null;
            else return n.minimum();
        }

        public abstract bool insert(ChildPtr _ref, byte[] key, object value, int depth, bool force_clone);

        public static bool insert(Node n, ChildPtr _ref, byte[] key, object value, int depth, bool force_clone)
        {
            // If we are at a NULL node, inject a leaf
            if (n == null)
            {
                _ref.change(new Leaf(key, value));
                return true;
            }
            else
            {
                return n.insert(_ref, key, value, depth, force_clone);
            }
        }

        public abstract bool delete(ChildPtr _ref, byte[] key, int depth, bool force_clone);

        public abstract int decrement_refcount();

        public abstract bool exhausted(int i);

        public static bool exhausted(Node n, int i)
        {
            if (n == null) return true;
            else return n.exhausted(i);
        }

        public static int to_uint(byte b)
        {
            return b & 0xFF;
        }

    }
}
